(function ($) {
    "use strict";
    //variables
    var header = $("#header"),
        h = $(window).height();
    //preloader
    $('#wrapper').on( "click", ".nav-link", function(event){
        event.preventDefault();
        var linkLocation = this.href;
        $('body').removeClass('ready-page');
        setTimeout(function() {
            window.location = linkLocation;
        }, 500);
    });
    setTimeout(function() {
        $('body').addClass('ready-page');
    }, 0);

    // slick initialisation
    if($(".comments-slider").length) {
        $(".comments-slider").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: true,
            infinite: true,
            arrows: false,
            dots: true
        });
    }
    // header mobile menu show-hide
    header.find(".menu-opener-js").on("click", function () {
        $(this).toggleClass("active").closest(header).toggleClass("menu-active");
    });
    // form plagin initialisation
    if($("form").length){
        //jquery custom form
        jcf.replaceAll();
        //form validation
        jQuery('form').validate();
    }
    //scroll functions

    animationSection();

    $( window ).scroll(function() {
        setTimeout(function(){
        animationSection();
        }, 700);
    });
    function  animationSection(){
        $(".section").each(function () {
            if (($(window).scrollTop() + h) >= $(this).offset().top + 200) {
                $(this).addClass("animation");
            }else{
                $(this).removeClass("animation");
            }
        });
    }
})(jQuery);
