var gulp        	= require('gulp'),
	syntax          = 'sass',
	injectPartials = require('gulp-inject-partials'),
    sass        	= require('gulp-sass'),
    browserSync 	= require('browser-sync'),
    imagemin    	= require('gulp-imagemin'),
    pngquant        = require('imagemin-pngquant'),
    cache       	= require('gulp-cache'),
    autoprefixer 	= require('gulp-autoprefixer'),
	gutil           = require('gulp-util'),
    gcmq            = require('gulp-group-css-media-queries'),
    plumber         = require('gulp-plumber'),
    sourcemaps      = require('gulp-sourcemaps'),
    cssbeautify     = require('gulp-cssbeautify'),
    concat          = require('gulp-concat'),
    uglify          = require('gulp-uglifyjs'),
    cssnano         = require('gulp-cssnano');

gulp.task('styles', function() {
	return gulp.src('src/scss/main.'+syntax+'')
	.pipe(plumber(function(error) {
	  gutil.log(gutil.colors.bold.red(error.message));
	  gutil.beep();
	  this.emit('end');
	 }))
	.pipe(sourcemaps.init())
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefixer(['last 2 versions'], {cascade: true}))
	.pipe(sourcemaps.write('./'))
	.pipe(plumber.stop())
	.pipe(gulp.dest('build/css'))
	.pipe(browserSync.reload({stream: true}))
});

gulp.task('scripts', function(){
	return gulp.src([
		'node_modules/jquery/dist/jquery.min.js',// Подключаем jQuery
		'node_modules/jquery-validation/dist/jquery.validate.js', // Подключаем jQuery Validation
		'node_modules/slick-carousel/slick/slick.min.js', // Подключаем Slick
		'node_modules/jcf/dist/js/jcf.js', // Подключаем JCF
		'node_modules/jcf/dist/js/jcf.select.js', // Подключаем JCF-SELECT
		'src/js/scripts.js'// Подключаем кастомные стили
	])
	.pipe(sourcemaps.init())
	.pipe(plumber())
	.pipe(concat('common.js'))
	.pipe(uglify())
	.pipe(gulp.dest('build/js'))
	.pipe(sourcemaps.write('source-maps'))
	.pipe(browserSync.reload({stream: true}))
});

gulp.task('css-libs', function() {
	return gulp.src([
		'node_modules/@fortawesome/fontawesome-free/css/all.css', // подключение font-awesome
		'node_modules/slick-carousel/slick/slick-theme.css', // подключение стилей slick
		'node_modules/slick-carousel/slick/slick.css', // подключение стилей slick
	])
	.pipe(concat('libs.min.css'))
	.pipe(cssnano())
	.pipe(gulp.dest('build/css'));
});

gulp.task('browser-sync', function() { //создание локального сервера
	browserSync({
		server: {
			baseDir: './',
			index: "index.html",
            directory: true
		},
		watchTask: true
	});
});

gulp.task('build', ['img', 'styles'], function() {
	var buildCSS = gulp.src([
		'src/css/main.css',
		'src/css/libs.min.css'
	])
		.pipe(cssbeautify())
 		.pipe(gcmq())
		.pipe(gulp.dest('build/css'));

	var buildFonts = gulp.src('src/fonts/**/*')
		.pipe(gulp.dest('build/fonts'));

	var buildJS = gulp.src('src/js/common.js')
		.pipe(gulp.dest('build/js'));

});

gulp.task('img', function() {
	return gulp.src('src/images/**/*')
	.pipe(cache(imagemin({
		interlaced: true,
		progressive: true,
		svgoPlugins: [{removeViewBox: false}],
		une: [pngquant()]
	})))
	.pipe(gulp.dest('build/images'));
});


gulp.task('partial', function () {
	return gulp.src('./src/templates/*.html')
		.pipe(injectPartials())
		.pipe(gulp.dest('./build'));
});

gulp.task('watch', ['browser-sync', 'build', 'styles', 'css-libs', 'scripts' , 'partial'] , function() {

	gulp.watch('src/templates/parts/*.html', ['partial' , browserSync.reload]);
	gulp.watch('src/templates/*.html', ['partial' , browserSync.reload]);
	gulp.watch('src/scss/**/*.'+syntax+'', ['styles' , 'build' ,browserSync.reload]);
	gulp.watch(['src/js/*.js', '!js/common.js'], ['scripts' , 'build' ,browserSync.reload]);

});
